//
//  GameScene.swift
//  flappycow
//
//  Created by Terry Cheong on 2018/8/29.
//  Copyright © 2018 Terry Cheong. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate
{
    var is_game_started = false
    var is_dead = false
    var score = Int(0)
    
    var score_label = SKLabelNode()
    var highscore_label = SKLabelNode()
    var tap_to_play_label = SKLabelNode()
    var scoreboard = SKSpriteNode()
    var pause_btn = SKSpriteNode()
    var logo_img = SKSpriteNode()
    var wall_pair = SKNode()
    var move_and_remove = SKAction()
    
    let cow_altas = SKTextureAtlas(named:"player")
    var cow_sprites : Array<SKTexture> = []
    var cow = SKSpriteNode()
    var repeat_action_cow = SKAction()
    
    var restart_btn = SKSpriteNode()
    
    
    func create_scene()
    {   self.physicsBody = SKPhysicsBody(edgeLoopFrom: self.frame)
        
        self.physicsBody?.isDynamic = true
        self.physicsBody?.affectedByGravity = false
        self.physicsWorld.contactDelegate = self

        
        //----------------------------
        // Texture related
        //----------------------------
        self.backgroundColor = SKColor(red: 80.0/255.0, green: 192.0/255.0, blue: 203.0/255.0, alpha: 1.0)
        let green = SKColor(red: 94.0/255.0, green: 226.0/255.0, blue: 112.0/255.0, alpha: 1.0)
        let ground_texture = SKTexture(imageNamed: "land")
        let sky_texture = SKTexture(imageNamed: "sky")
        // build background
        for i in 0..<2
        {
            let ground = SKSpriteNode(texture: ground_texture)
            let sky = SKSpriteNode(texture: sky_texture)
            sky.zPosition = -20
            sky.setScale(2.0)
            sky.anchorPoint = CGPoint.init(x:0, y:0)
            sky.position = CGPoint(x: CGFloat(i) * sky.size.width, y: self.frame.height/2 - sky.size.height/2)
            sky.name = "sky"
            
            
            ground.setScale(2.0)
            ground.anchorPoint = CGPoint.init(x:0, y:0)
            ground.size = CGSize(width: self.frame.width, height:0.2 * self.frame.height)
            ground.position = CGPoint(x: CGFloat(i) * ground.size.width, y: 0)
            ground.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: ground.size.width, height: ground.size.height * 2.33333))
        
            ground.name = "land"
            
            let ss = SKSpriteNode(color: green, size: CGSize(width: self.frame.width, height:self.frame.height/2-sky.size.height/2-ground.size.height))
            ss.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2 - sky.size.height/2 - ss.size.height/2)
            
            ground.physicsBody?.categoryBitMask = CollisionBitMask.groundCategory
            ground.physicsBody?.collisionBitMask = CollisionBitMask.birdCategory
            ground.physicsBody?.contactTestBitMask = CollisionBitMask.birdCategory
            ground.physicsBody?.isDynamic = false
            ground.physicsBody?.affectedByGravity = false


            ground.zPosition = 3 
            
            self.addChild(sky)
            self.addChild(ground)
            self.addChild(ss)
        }
        
        for i in 1..<4
        { cow_sprites.append(cow_altas.textureNamed(String(format: "%@-%02d", "bird", i)))}
        cow_sprites.append(cow_altas.textureNamed("bird-02"))
        
        self.cow = create_cow()
        self.addChild(cow)
        
        let animation = SKAction.animate(with: self.cow_sprites, timePerFrame: 0.1)
        
        self.repeat_action_cow = SKAction.repeatForever(animation)
        
    }
    
    func create_cow() -> SKSpriteNode
    {   let cow = SKSpriteNode(texture: SKTextureAtlas(named: "player").textureNamed("bird-01"))
        cow.position = CGPoint(x: self.frame.midX, y: self.frame.midY)
        
        cow.zPosition = 100
        
        cow.size = CGSize(width: 90, height: 90 * cow.size.height/cow.size.width)
        cow.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: cow.size.width-35, height: cow.size.height-35))
        cow.physicsBody?.linearDamping = 1.1
        cow.physicsBody?.restitution = 0
        
        cow.physicsBody?.categoryBitMask = CollisionBitMask.birdCategory
        cow.physicsBody?.collisionBitMask = CollisionBitMask.pillarCategory | CollisionBitMask.groundCategory
        cow.physicsBody?.contactTestBitMask = CollisionBitMask.pillarCategory | CollisionBitMask.flowerCategory | CollisionBitMask.groundCategory
        
        cow.physicsBody?.affectedByGravity = false
        cow.physicsBody?.isDynamic = true
        
        return cow
    }
    
    override func didMove(to view: SKView)
    {
        create_scene()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        if is_game_started == false
        {
            is_game_started = true
            cow.physicsBody?.affectedByGravity = true
            
            self.cow.run(repeat_action_cow)
            
            let spawn = SKAction.run ({
                () in
                self.wall_pair = self.createWalls()
                self.addChild(self.wall_pair)
            })
            
            score_label = create_score_label()
            self.addChild(score_label)
            
            
            let delay = SKAction.wait(forDuration: 1.5)
            let spawn_delay = SKAction.sequence([spawn, delay])
            
            self.run(SKAction.repeatForever(spawn_delay))
            
            let distance = CGFloat(self.frame.width + wall_pair.frame.width)
            let move_walls = SKAction.moveBy(x: -distance-50, y: 0, duration: TimeInterval(0.008 * distance))
            let remove_walls = SKAction.removeFromParent()
            
            move_and_remove = SKAction.sequence([move_walls, remove_walls])
            
            cow.physicsBody?.velocity = CGVector(dx: 0, dy: 0)
            cow.physicsBody?.angularVelocity = 0
            cow.physicsBody?.applyImpulse(CGVector(dx: 0, dy: 23))
            
         //   cow.physicsBody?.applyTorque(-0.05)
        }
        else
        {
            if is_dead == false {
                cow.physicsBody?.velocity = CGVector(dx: 0, dy: 0)
                cow.physicsBody?.angularVelocity = 0
                cow.physicsBody?.applyImpulse(CGVector(dx: 0, dy: 23))
         //       cow.physicsBody?.applyTorque(-0.05)

            }
        }
        
        for touch in touches
        {   let location = touch.location(in: self)
            
            if restart_btn.contains(location) { restartScene() }
        }
    }
    
    override func update(_ currentTime: TimeInterval)
    {
        if is_game_started
        {   if is_dead == false
            {
                enumerateChildNodes(withName: "sky", using: ({(node, error) in
                    let bg = node as! SKSpriteNode
                    bg.position.x -= 2
                    
                    if (bg.position.x <= -bg.size.width) {
                        bg.position.x +=  bg.size.width * 2
                    }
                }))
                
                enumerateChildNodes(withName: "land", using: ({(node, error) in
                    let bg = node as! SKSpriteNode
                    bg.position.x -= 2
                    
                    if (bg.position.x <= -bg.size.width) {
                        bg.position.x +=  bg.size.width * 2
                    }
                }))

            }
        }
    }
    
    func didBegin(_ contact: SKPhysicsContact)
    {
        let first = contact.bodyA, second = contact.bodyB
        
        if (first.categoryBitMask == CollisionBitMask.birdCategory || second.categoryBitMask == CollisionBitMask.birdCategory )
        {
            
            if (first.categoryBitMask == CollisionBitMask.flowerCategory || second.categoryBitMask == CollisionBitMask.flowerCategory)
            {
                self.score += 1

                let font = UIFont(name: "HelveticaNeue-Bold", size: 50)
                let attributes:[NSAttributedStringKey:Any] = [.strokeColor: UIColor.darkGray, .strokeWidth: -3.0, .font: font!, .foregroundColor:UIColor.white]
                
                score_label.attributedText = NSMutableAttributedString(string: " \(score) ", attributes: attributes)                
            }
            else {
                enumerateChildNodes(withName: "wallPair", using: ({(node, err) in
                    node.speed = 0
                    self.removeAllActions()
                }))
                
                if (is_dead == false)
                {   is_dead = true
                    self.cow.physicsBody?.isDynamic = false
                    self.cow.removeAllActions()
                    self.score_label.removeFromParent()
                    
                    if UserDefaults.standard.object(forKey: "highscore") != nil
                    {   let highscore = UserDefaults.standard.integer(forKey: "highscore")
                        
                        UserDefaults.standard.set(max(highscore, score), forKey: "highscore")
                    }
                    else { UserDefaults.standard.set( score, forKey: "highscore") }

                    
                    create_scoreboard()
                    print("Score = ", self.score)
                }
            }
        }
    }
    
    func restartScene(){
        self.removeAllChildren()
        self.removeAllActions()
        is_dead = false
        is_game_started = false
        score = 0
        create_scene()
    }
}

