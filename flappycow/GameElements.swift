//
//  GameElements.swift
//  flappycow
//
//  Created by Terry Cheong on 2018/8/29.
//  Copyright © 2018 Terry Cheong. All rights reserved.
//

import SpriteKit


struct CollisionBitMask {
    static let birdCategory:UInt32 = 0x1 << 0
    static let pillarCategory:UInt32 = 0x1 << 1
    static let flowerCategory:UInt32 = 0x1 << 2
    static let groundCategory:UInt32 = 0x1 << 3
}

extension GameScene
{
    func create_scoreboard()
    {
        scoreboard = SKSpriteNode(imageNamed: "scoreboard")
        scoreboard.size = CGSize(width: scoreboard.size.width * 1.3, height: scoreboard.size.height * 1.3)
        scoreboard.position = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
        scoreboard.zPosition = 999
        
        restart_btn = SKSpriteNode(imageNamed: "playbtn")
        restart_btn.size = CGSize(width: scoreboard.size.width / 3, height: restart_btn.size.height/restart_btn.size.width * scoreboard.size.width / 3)
        restart_btn.position = CGPoint(x: self.frame.width/2, y: self.frame.height / 2 - scoreboard.size.height/2)
        restart_btn.zPosition = 999

        let score_lbl = SKLabelNode()
        
        highscore_label = SKLabelNode()
        let font = UIFont(name: "HelveticaNeue-Bold", size: 20)
        let attributes:[NSAttributedStringKey:Any] = [.strokeColor: UIColor.darkGray, .strokeWidth: -3.0, .font: font!,
                                                      .foregroundColor:UIColor.white]
        
        let highscore = UserDefaults.standard.integer(forKey: "highscore")
        
        
        highscore_label.text = " \(highscore) "
        highscore_label.attributedText = NSMutableAttributedString(string: " \(highscore) ", attributes: attributes)
        
        highscore_label.position = CGPoint(x: scoreboard.size.width/3, y: -scoreboard.size.width/12)
        highscore_label.zPosition = 1001
        
        score_lbl.text = " \(score) "
        score_lbl.attributedText = NSMutableAttributedString(string: " \(score) ", attributes: attributes)

        score_lbl.position = CGPoint(x: scoreboard.size.width/3, y: scoreboard.size.width/12)
        score_lbl.zPosition = 1001


        scoreboard.setScale(0)
        
        scoreboard.addChild(highscore_label)
        scoreboard.addChild(score_lbl)

        self.addChild(scoreboard)
        self.addChild(restart_btn)
        scoreboard.run(SKAction.scale(to: 1.0, duration: 0.3))
        
        
        let score_label = SKSpriteNode()
        score_label.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2 - scoreboard.size.height)
    }
    
    
    func createWalls() -> SKNode
    {
        
        let top_pipe = SKSpriteNode(imageNamed: "PipeDown");
        top_pipe.position = CGPoint(x: self.frame.width + 25, y: self.frame.height/2 + 250)
        top_pipe.setScale(2.0)

        
        let bot_pipe = SKSpriteNode(imageNamed: "PipeUp")
        
        bot_pipe.position = CGPoint(x: self.frame.width + 25, y: self.frame.height/2 - 250)
        bot_pipe.setScale(2.0)
        
        let score_node = SKSpriteNode()
        score_node.size = CGSize(width: top_pipe.size.width, height: 150)
        
        score_node.position = CGPoint(x: self.frame.width + 25, y: self.frame.height / 2)
        
        score_node.physicsBody = SKPhysicsBody(rectangleOf: score_node.size)
        
        score_node.physicsBody?.affectedByGravity = false
        score_node.physicsBody?.isDynamic = false
        score_node.physicsBody?.categoryBitMask = CollisionBitMask.flowerCategory
        score_node.physicsBody?.collisionBitMask = 0
        score_node.physicsBody?.contactTestBitMask = CollisionBitMask.birdCategory
                
        
        wall_pair = SKNode()
        wall_pair.name = "wallPair"
        
        
        top_pipe.physicsBody = SKPhysicsBody(rectangleOf: top_pipe.size)
        top_pipe.physicsBody?.categoryBitMask = CollisionBitMask.pillarCategory
        top_pipe.physicsBody?.collisionBitMask = CollisionBitMask.birdCategory
        top_pipe.physicsBody?.contactTestBitMask = CollisionBitMask.birdCategory

        top_pipe.physicsBody?.affectedByGravity = false
        top_pipe.physicsBody?.isDynamic = false
        
        bot_pipe.physicsBody = SKPhysicsBody(rectangleOf: bot_pipe.size)
        bot_pipe.physicsBody?.categoryBitMask = CollisionBitMask.pillarCategory
        bot_pipe.physicsBody?.collisionBitMask = CollisionBitMask.birdCategory
        bot_pipe.physicsBody?.contactTestBitMask = CollisionBitMask.birdCategory
        bot_pipe.physicsBody?.affectedByGravity = false
        bot_pipe.physicsBody?.isDynamic = false
        
        wall_pair.addChild(score_node)
        wall_pair.addChild(top_pipe)
        wall_pair.addChild(bot_pipe)
        
        wall_pair.zPosition = 1
        
        let ground = self.childNode(withName: "land") as! SKSpriteNode
        
        let bound = -((self.frame.height/2 - 250) + bot_pipe.size.height/2 - ground.size.height - 50)
        let up_bound = (self.frame.height-50-(self.frame.height/2 + 250 - top_pipe.size.height/2))
        
        print(bound, ground.size.height)
        
        let hole_pos = random(min:bound, max: up_bound)
        wall_pair.position.y += hole_pos
        
        wall_pair.run(move_and_remove)
        
        return wall_pair
    }
    
    func create_score_label() -> SKLabelNode
    {
        let score_label = SKLabelNode()
        score_label.position = CGPoint(x: self.frame.width/2, y:self.frame.height / 2 + self.frame.height / 2.6)
        score_label.zPosition = 5
        score_label.color = SKColor.white
        
        let font = UIFont(name: "HelveticaNeue-Bold", size: 50)
        let attributes:[NSAttributedStringKey:Any] = [.strokeColor: UIColor.darkGray, .strokeWidth: -3.0, .font: font!,
                                                      .foregroundColor:UIColor.white]
        
        score_label.text = " \(score) "
        score_label.attributedText = NSMutableAttributedString(string: " \(score) ", attributes: attributes)
        
        return score_label
    }

    func random() -> CGFloat{
        return CGFloat(Float(arc4random()) / 0xFFFFFFFF)
    }
    func random(min : CGFloat, max : CGFloat) -> CGFloat{
        return random() * (max - min) + min
    }
    
}


